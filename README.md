# sync_pulse

Brian Tremaine 06/01/2018

Python scripts using WinPython V3.6.5 on a Windows 10 64-bit system.

File sync_pulse.py reads a pico-scope CSV file and parses the data into the
the times from each window rising edge to the pulses within the window.


