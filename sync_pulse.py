# TremaineConsultingGroup
# Brian Tremaine
# April 2018
# Process Wall sync pulse data from Picoscope CSV file
#
""" Process picoscope CSV file extracting sync pulse timing
    


"""
import os
from operator import xor
import matplotlib.pyplot as plt
import numpy as np
#import matplotlib.pyplot as plt
#import time
#import argparse
import logging, sys
import argparse
import csv


def getSync(start, tme, win, pulse):
    """ detect sync pulses
    """
    # loop through high window to find pulse starting edge
    tp = [] # initialize to list
    j = 0
    while win[start+j] == 1:
       #logging.info([start, start+j, pulse[start+j]])
       if pulse[start+j] & ~pulse[start+j-1]:
          tp.append(tme[start+j])
          
       j = j+1
    
    return tp


def findWindows(win):
    """ find rising edge of win
        return value, wd, is a list of indexes
    """
    wd = [] # initialize to list 
    w2= [0] + win
    w2 = w2[:-1]
    
    j= 0
    for i in range(len(win)):
        if win[i] & ~w2[i] :
            wd.append(i)
            j = j+1
    
    return wd

def cnvrtTimesNsec(Tstart, tp):
    """ convert absolute times of variable length to delta times
    """ 
    tdelta = [] # initialize as list
    for i in range(len(tp)):
        if i == 0:
            Tlast = Tstart
        else:
            Tlast = tp[i-1]
            
        tdelta.append(int(1e6*(tp[i] - Tlast)))
    
    return tdelta


""" ============ main ======================================
"""
if __name__ == '__main__':
    
  logging.basicConfig(filename='sync.log',filemode='w',level=logging.DEBUG)
  logging.info("Program started") 
  
  # construct the argument parse and parse the arguments
  ap = argparse.ArgumentParser()
  ap.add_argument("-f", "--file", required=True,
	help="path to input csv file")
  args = vars(ap.parse_args())
 
    
  
  # Load the CSV file for processing
  # Columns:
  # Time, ChA, D0, D1, D2, D3, D4, D5, D6, D7
  data = []
  Time = list()
  ChA = list()
  D2 = list()
  D3 = list()
  with open(args['file'], newline='') as csvfile:
     print("Headers found:")
     filereader = csv.reader(csvfile, delimiter=',') #, quotechar='|')
     line = 0
     for row in filereader:
         if line == 0:
             print(row)          
         if line == 1:
             print(row) 
         if line > 2:
             data = row
             Time.append(row[0])
             ChA.append(row[1])
             D2.append(row[4])
             D3.append(row[5])
                          
         line = line + 1  
         
  # convert str to float       
  Time = [float(i) for i in Time]
  ChA = [float(i) for i in ChA]
  pulse = [int(i) for i in D2]
  Win = [int(i) for i in D3]
     
  # plot for sanity check     
  W1 = 373800  
  W2 = W1 + 6000
  Tme = Time[W1:W2]
  plt.figure()
  plt.title('Channel A')
  plt.plot(Tme,ChA[W1:W2])
  
  plt.figure()
  plt.title('pulse')
  plt.plot(Tme,pulse[W1:W2])
  
  plt.figure()
  plt.title('window')
  plt.plot(Tme,Win[W1:W2])
  
  plt.show()
  
  
  """ Compute delta time on sync pulses and Dynamic pulses
  """
  
  # list of window start times as indexes
  wd = findWindows(Win)
  
  if len(wd) is 0:
      raise Exception('exit, no window edges found.')
      
  # get all pulse times [t0, t1, t2, t3, ...] 
  # and save to csv file\
  
  data = [] # initialize list for rows
  for i in range(len(wd)):     
      tp = getSync(wd[i], Time, Win, pulse) # returns times
      # convert to delta times, tp variable length
      tdelta = cnvrtTimesNsec(Time[wd[i]], tp)       
      data.append(tdelta)
      
   # save to csv file
  myFile = open('results.csv', 'w', newline='')  
  with myFile:  
      writer = csv.writer(myFile)
      writer.writerows(data)


        
        

  
  
  
  
  
